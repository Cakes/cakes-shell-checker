package org.cake.shellchecker;

import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class Checker {
	private ArrayList<Filters> filters = new ArrayList<>();
	private final URL url;

	public Checker(URL url) {
		this.url = url;

		if (this.validateUrl())
			this.validate();

	}

	private boolean validateUrl() {
		for (String s : endsWith)
			if (url.getHost().toLowerCase().endsWith(s.toLowerCase()))
				return false;
		return true;
	}

	private boolean validate() {
		String s = Methods.downloadString(getUrl()).toLowerCase();

		for (Filters tf : Filters.values())
			for (String filter : tf.getFilters())
				if (Pattern.compile(filter.toLowerCase(), Pattern.DOTALL).matcher(s).find() && !filters.contains(tf))
					filters.add(tf);

		return (filters.contains(Filters.POST) || filters.contains(Filters.GET)) && (filters.contains(Filters.UDP) || filters.contains(Filters.TCP));
	}

	public boolean isGET() {
		return filters.contains(Filters.GET);
	}

	public boolean isPost() {
		return filters.contains(Filters.POST);
	}

	public boolean isUDP() {
		return filters.contains(Filters.UDP);
	}

	public boolean isTCP() {
		return filters.contains(Filters.TCP);
	}

	public URL getUrl() {
		return url;
	}

	public static ArrayList<String> endsWith = new ArrayList<String>() {
		private static final long serialVersionUID = 1L;

		{
			add("webs.com");
			add("blogspot.no");
			add("blogspot.com");
			add("blogspot.se");
			add("xhamster.com");
			add("youtu.be");
			add("youtube.com");
			add("pastebin.com");
			add("pastee.com");
			add("ideone.com");
			add("safebin.net");
			add("gist.github.com");
			add("0bin.net");
			add("Slexy.org");
			add("wordpress.com");
			add("wordpress.org");
			add("wordpress.no");
		}
	};
}