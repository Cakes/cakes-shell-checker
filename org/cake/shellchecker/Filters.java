package org.cake.shellchecker;

public enum Filters {
	GET("method(\\s+|)=(\\s+|)(\"|)GET(\"|)"), POST("method(\\s+|)=(\\s+|)(\"|)POST(\"|)"), INPUT("<input\\s+"), UDP("<b>UDP ((d|)dos|Flood)<\\/b>",
			"value=\\\"phptools\\\""), TCP("(Don\'t DoS yourself|Port[\\:|\"])");

	private Filters(String... filters) {
		this.filters = filters;
	}

	public String[] getFilters() {
		return filters;
	}

	private String[] filters;
}
