package org.cake.shellchecker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class Methods {

	public static String downloadString(URL url) {
		StringBuilder str = new StringBuilder();

		try {
			URLConnection uc = url.openConnection();

			uc.setConnectTimeout(Settings.TIMEOUT);
			uc.setReadTimeout(Settings.TIMEOUT);
			uc.setUseCaches(false);
			uc.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.13 (KHTML, like Gecko) Chrome/24.0.1284.0 Safari/537.13");

			BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
			String inputLine;

			while ((inputLine = in.readLine()) != null)
				str.append(inputLine + '\n');

			in.close();
		} catch (IOException e) {
			return str.toString();
		}

		return str.toString();
	}
}
