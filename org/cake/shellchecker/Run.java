package org.cake.shellchecker;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.JTextComponent;

import com.alee.extended.label.WebLinkLabel;
import com.alee.laf.WebLookAndFeel;
import com.alee.laf.button.WebToggleButton;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.progressbar.WebProgressBar;
import com.alee.laf.slider.WebSlider;
import com.alee.laf.text.WebTextArea;

public class Run extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private WebToggleButton start;
	private WebTextArea ucpGet, ucpPost, unchecked, tcpPost, tcpGet;
	private WebSlider slider1, slider2;
	private WebProgressBar progressBar;
	private JLabel lblDevolopedByCake, label, lblThreads, lblTimeoutseconds;

	private ConcurrentLinkedQueue<String> list = new ConcurrentLinkedQueue<>();
	private ExecutorService exec;
	private int worked = 0, current = 0, done = 0;

	private static double version = 0.201;

	private Rectangle open = new Rectangle(100, 100, 573, 500), closed = new Rectangle(100, 100, 573, 458);
	private Rectangle labelopen = new Rectangle(10, 448, 531, 16), labelclosed = new Rectangle(10, 410, 531, 16);

	public static void main(String[] args) {
		WebLookAndFeel.install();

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Run frame = new Run();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Run() {
		super("Cakes Shell Checker ~ v" + version);
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 573, 503);

		JScrollPane scrollPane = new JScrollPane();
		JScrollPane scrollPane_1 = new JScrollPane();
		JScrollPane scrollPane_2 = new JScrollPane();
		JScrollPane scrollPane_4 = new JScrollPane();
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane.setBounds(10, 23, 171, 338);
		scrollPane_1.setBounds(189, 23, 171, 155);
		scrollPane_2.setBounds(189, 206, 171, 155);
		scrollPane_3.setBounds(370, 23, 171, 155);
		scrollPane_4.setBounds(370, 206, 171, 155);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_3.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_4.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		contentPane = new JPanel(null);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		start = new WebToggleButton("Start");
		start.setShadeColor(Color.LIGHT_GRAY);
		start.setInnerShadeWidth(0);
		start.setRound(0);
		start.addActionListener(this);
		start.setBounds(10, 379, 171, 23);

		lblDevolopedByCake = new JLabel("<html>Devoloped by Cake, bitbucket#Cakes ~ 2012; L&F is weblookandfeel.");

		lblDevolopedByCake.setFont(UIManager.getFont("TextArea.font"));
		lblDevolopedByCake.setHorizontalAlignment(SwingConstants.CENTER);
		lblDevolopedByCake.setBounds(10, 410, 531, 16);
		unchecked = new WebTextArea();
		unchecked.setFont(new Font("Monospaced", Font.PLAIN, 11));
		scrollPane.setViewportView(unchecked);

		JLabel lblUnchecked = new JLabel("Unchecked:");
		lblUnchecked.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblUnchecked.setBounds(10, 11, 67, 14);

		JLabel lblUcp = new JLabel("UCP [GET]:");
		lblUcp.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblUcp.setBounds(189, 11, 67, 14);

		ucpGet = new WebTextArea();
		ucpGet.setFont(new Font("Monospaced", Font.PLAIN, 11));
		ucpGet.setText("");
		ucpGet.setEditable(false);
		scrollPane_1.setViewportView(ucpGet);

		ucpPost = new WebTextArea();
		ucpPost.setFont(new Font("Monospaced", Font.PLAIN, 11));
		ucpPost.setEditable(false);
		scrollPane_2.setViewportView(ucpPost);

		JLabel lblUcppost = new JLabel("UCP [POST]:");
		lblUcppost.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblUcppost.setBounds(191, 193, 67, 14);

		progressBar = new WebProgressBar();
		progressBar.setRound(0);
		progressBar.setIndeterminate(false);
		progressBar.setMinimum(0);
		progressBar.setMaximum(1);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		progressBar.setBounds(10, 410, 171, 23);

		lblThreads = new JLabel("Threads (2):");
		lblThreads.setBounds(189, 393, 162, 16);

		lblTimeoutseconds = new JLabel("Timeout (5):");
		lblTimeoutseconds.setBounds(370, 393, 171, 16);
		contentPane.add(lblTimeoutseconds);

		label = new JLabel("00/00");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(193, 379, 244, 16);

		slider1 = new WebSlider(WebSlider.HORIZONTAL);

		slider1.setMinimum(1);
		slider1.setMaximum(70);
		slider1.setMinorTickSpacing(0);
		slider1.setMajorTickSpacing(9);
		slider1.setValue(1);
		slider1.setBounds(182, 398, 178, 46);
		slider1.setVisible(false);

		slider2 = new WebSlider(WebSlider.HORIZONTAL);
		slider2.setSharpThumbAngle(false);
		slider2.setMinimum(1);
		slider2.setMaximum(12);
		slider2.setMinorTickSpacing(0);
		slider2.setMajorTickSpacing(1);
		slider2.setValue(5);
		slider2.setBounds(363, 398, 178, 46);

		JLabel label_1 = new JLabel("TCP [GET]:");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_1.setBounds(370, 11, 67, 14);

		tcpGet = new WebTextArea();
		tcpGet.setFont(new Font("Monospaced", Font.PLAIN, 11));
		tcpGet.setEditable(false);
		scrollPane_3.setViewportView(tcpGet);

		JLabel label_2 = new JLabel("TCP [POST]:");
		label_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_2.setBounds(372, 193, 67, 14);

		tcpPost = new WebTextArea();
		tcpPost.setFont(new Font("Monospaced", Font.PLAIN, 11));
		tcpPost.setEditable(false);
		scrollPane_4.setViewportView(tcpPost);

		contentPane.add(start);
		contentPane.add(slider2);
		contentPane.add(slider1);
		contentPane.add(progressBar);
		contentPane.add(label);
		contentPane.add(label_1);
		contentPane.add(label_2);
		contentPane.add(lblThreads);
		contentPane.add(lblUcppost);
		contentPane.add(lblUcp);
		contentPane.add(lblUnchecked);
		contentPane.add(lblDevolopedByCake);
		contentPane.add(scrollPane);
		contentPane.add(scrollPane_1);
		contentPane.add(scrollPane_2);
		contentPane.add(scrollPane_3);
		contentPane.add(scrollPane_4);

		addListeners();
		updateProcess();
	}

	public void actionPerformed(ActionEvent ae) {
		if (!(ae.getSource() instanceof WebToggleButton))
			return;
		if (ae.getActionCommand().equals("Start")) {
			if (unchecked.getText().trim().isEmpty()) {
				WebOptionPane.showMessageDialog(null, "Missing shells from the unchecked text area.", "Error", WebOptionPane.ERROR_MESSAGE);
				return;
			}

			if (unchecked.getText().split("\n").length < slider1.getValue()) {
				WebOptionPane.showMessageDialog(null, "Cannot have less thread values then existing shells..", "Error", WebOptionPane.ERROR_MESSAGE);
				return;
			}

			try {
				start((WebToggleButton) (ae.getSource()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void start(final WebToggleButton button) throws InterruptedException, ExecutionException, MalformedURLException {
		// TODO: Redo.
		done = 0;
		worked = 0;

		updateCurrent();
		updateProcess();

		button.setEnabled(false);
		unchecked.setEnabled(false);

		ucpGet.setText("");
		tcpGet.setText("");
		ucpPost.setText("");
		tcpPost.setText("");

		HashSet<String> set = new HashSet<>();

		for (final String s : unchecked.getText().split("\n"))
			if (!s.trim().isEmpty())
				set.add(s.trim());

		String s = "";

		for (String string : set) 
			s += string + '\n';
		

		unchecked.setText(s);

		list.addAll(set);

		exec = Executors.newFixedThreadPool(Settings.THREADS);

		for (int i = 0; i < slider1.getValue(); i++) {

			if ((current != 0 && done != current) || !list.isEmpty())
				exec.submit(new Runnable() {

					@Override
					public void run() {
						while (!list.isEmpty()) {
							String s = list.poll();
							if (s != null) {
								try {
									Checker checker = new Checker(new URL(s));

									if (checker.isUDP()) {
										if (checker.isGET() && !ucpGet.getText().contains(checker.getUrl().toString())) {
											ucpGet.setText(ucpGet.getText().concat(checker.getUrl().toString()).concat("\n"));
											worked++;
										} else if (checker.isPost() && !ucpPost.getText().contains(checker.getUrl().toString())) {
											ucpPost.setText(ucpPost.getText().concat(checker.getUrl().toString()).concat("\n"));
											worked++;
										}
									} else if (checker.isTCP()) {
										if (checker.isGET() && !tcpGet.getText().contains(checker.getUrl().toString())) {
											tcpGet.setText(tcpGet.getText().concat(checker.getUrl().toString()).concat("\n"));
											worked++;
										} else if (checker.isPost() && !tcpPost.getText().contains(checker.getUrl().toString())) {
											tcpPost.setText(tcpPost.getText().concat(checker.getUrl().toString()).concat("\n"));
											worked++;
										}
									}
								} catch (MalformedURLException ignored) {
								}

								done++;

								updateProcess();

								if (done == current && list.isEmpty()) {
									button.setEnabled(true);
									unchecked.setEnabled(true);
									button.setSelected(false);
									progressBar.setVisible(false);
									updateProcess();
									exec.shutdownNow();
								}
							}
						}

					}
				});
		}

	}

	public void updateProcess() {
		if (slider1.getValue() == 1)
			if (current > 1)
				slider1.setValue(current > slider1.getMaximum() ? slider1.getMaximum() : current);

		progressBar.setValue(done);
		progressBar.setMaximum(current);
		progressBar.setVisible(done > 0);
		slider1.setVisible(current != 0);
		slider2.setVisible(current != 0);
		lblThreads.setVisible(current != 0);
		lblTimeoutseconds.setVisible(current != 0);

		label.setText(current != 0 ? String.format("Current: %s/%s", done, current) + (worked != 0 ? " - " + worked + " worked." : ".") : "");

		Rectangle curr = current != 0 ? open : closed;

		setSize(new Dimension(559, curr.height));
		lblDevolopedByCake.setBounds(current != 0 ? labelopen : labelclosed);

	}

	private void updateCurrent() {
		HashSet<String> map = new HashSet<>();

		current = 0;

		for (final String s : unchecked.getText().split("\n"))
			if (!s.trim().isEmpty())
				map.add(s.trim());
		current = map.size();
	}

	private void addListeners() {
		slider1.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				lblThreads.setText("Threads (" + slider1.getValue() + "):");
			}
		});

		slider2.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				Settings.TIMEOUT = slider2.getValue() * 1000;
				lblTimeoutseconds.setText("Timeout (" + slider2.getValue() + "):");
			}
		});

		tcpGet.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				tcpGet.selectAll();
			}
		});

		tcpPost.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				tcpPost.selectAll();
			}
		});

		unchecked.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent e) {

				updateCurrent();

				updateProcess();
			}

		});

		ucpGet.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				ucpGet.selectAll();
			}
		});

		ucpPost.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				ucpPost.selectAll();
			}
		});
	}
}